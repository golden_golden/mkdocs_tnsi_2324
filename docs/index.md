---
title : "Bienvenue"

---

# Bienvenue
 
Ce site présente les cours de NSI pour le niveau Terminale. Il a été conçu pour les élèves du lycée Bossuet à Condom.

<br>

Si vous êtes élèves, ce site vous permettra : de mettre votre classeur à jour, rattraper les cours que vous auriez manqués, vous exercer, avoir accès aux contenus multimédias diffusées en classe, consulté les sujets et les corrigés des DM et même pour les plus motivés à avoir accès à des contenues supplémentaires …

<br/>

<center>
<a href="https://golden_golden.gitlab.io/mkdocs_tnsi_2324/">
<img src="./images/accueil/qr-code.svg" alt="Qr code du site" style="width:300px;"/>
</a>
</center>