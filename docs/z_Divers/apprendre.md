---
title : "Apprendre"

---

# Comment mieux apprendre ?

## Introduction
<center>
    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/RVB3PBPxMWg?si=zKotAK7H25CJvw1_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>    



## Système Leitner
<center>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/xUS9PkPh54o?si=ZRs6RqaeJncaqMrV" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center> 


## Carte mentale

<center>
   <iframe width="560" height="315" src="https://www.youtube.com/embed/vHfiuageaHM?si=bk4lvN2uAYdHLq-4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center> 

<center>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/NhY23KS_iis?si=UhJipcHw1tHrc3NX" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

- [https://www.youtube.com/watch?v=mGWXkySpJ2I](https://www.youtube.com/watch?v=mGWXkySpJ2I)


-[https://www.youtube.com/watch?v=7lTSBNsPK4o](https://www.youtube.com/watch?v=7lTSBNsPK4o
)

## Les Techniques mnémoniques
(a faire : exemple de performances)

### Palais mental

<center> 
    <iframe width="560" height="315" src="https://www.youtube.com/embed/N9fz_071WX8?si=Q0gelkZQ9Dxep3aI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center> 

<center> 
    <iframe width="560" height="315" src="https://www.youtube.com/embed/cMGTLRgQQP0?si=mWI4-oaCq1NzZ7wd" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>
[autre exemple](https://www.youtube.com/watch?v=cZX29ao6F1o)
[exemple ludique](https://www.youtube.com/shorts/bQapUuuR53g)


### a trier et finir

- [https://www.youtube.com/watch?v=uatMYZe6tLQ](https://www.youtube.com/watch?v=uatMYZe6tLQ)
- [https://www.youtube.com/watch?v=z-ANQw-R90g](https://www.youtube.com/watch?v=z-ANQw-R90g)
- [https://www.youtube.com/watch?v=eIhlwJsrRRU](https://www.youtube.com/watch?v=eIhlwJsrRRU)
- [https://www.youtube.com/watch?v=FMrfPdiwk3A](https://www.youtube.com/watch?v=FMrfPdiwk3A)
- [https://www.youtube.com/watch?v=8dpR490wiGk](https://www.youtube.com/watch?v=8dpR490wiGk)
- [https://www.youtube.com/watch?v=uatMYZe6tLQ](https://www.youtube.com/watch?v=uatMYZe6tLQ)
