---
title: "Cahier de texte"

---


# Cahier de texte







!!! note "Semaine 35"

    **Séance 1 :**
    
    Contenu de la séance : 

    - *Présentation*
    - *Début du cours : [Mise au points des programmes](./Theme_5/1_Sommaire.md) 
    Étude des parties : introduction, types d'erreurs, coder beau, et début de documenter.*

    <br/>

    Travail donné :

    - *Lire les cours : sur les tests et sur les tests unitaires*
   
