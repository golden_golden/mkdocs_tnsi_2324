---
title : "Programme"

---

# Structures de données

*Extrait du programme*

L’écriture sur des exemples simples de plusieurs implémentations d’une même structure de
données permet de faire émerger les notions d’interface et d’implémentation, ou encore de
structure de données abstraite.

Le paradigme de la programmation objet peut être utilisé pour réaliser des implémentations
effectives des structures de données, même si ce n’est pas la seule façon de procéder.

Le lien est établi avec la notion de modularité qui figure dans la rubrique « langages et
programmation » en mettant en évidence l’intérêt d’utiliser des bibliothèques ou des API
*(Application Programming Interface).*


<br>

<table>
    <thead>
        <tr>
            <th >Contenus</th>
            <th >Capacités attendues</th>
            <th >Commentaires</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Structures de données, interface et implémentation.</td>
            <td>Spécifier une structure de données par son interface.<br>
            Distinguer interface et implémentation.<br>
            Écrire plusieurs implémentations d’une même structure de données</td>
            <td>L’abstraction des structures de données est introduite après plusieurs implémentations d’une structure simple comme la file (avec un tableau ou avec deux piles).</td>
        </tr>
        <tr>
            <td>Vocabulaire de la programmation objet : classes, attributs, méthodes, objets.</td>
            <td>Écrire la définition d’une classe.<br>
            Accéder aux attributs et méthodes d’une classe.</td>
            <td>On n’aborde pas ici tous les aspects de la programmation objet comme le polymorphisme et l’héritage.</td>
        </tr>
        <tr>
            <td>Listes, piles, files : structures linéaires. Dictionnaires, index et clé.</td>
            <td>Distinguer des structures par le jeu des méthodes qui les caractérisent.<br> Choisir une structure de données adaptée à la situation à modéliser.<br>
            Distinguer la recherche d’une valeur dans une liste et dans un dictionnaire.</td>
            <td>On distingue les modes FIFO (first in first out) et LIFO (last in first out) des piles et des files</td>
        </tr>
        <tr>
            <td>Arbres : structures hiérarchiques.<br>
            Arbres binaires : nœuds, racines, feuilles, sous-arbres gauches, sous-arbres droits</td>
            <td>Identifier des situations nécessitant une structure de données arborescente.<br>
            Évaluer quelques mesures des arbres binaires (taille, encadrement de la hauteur, etc.).</td>
            <td>On fait le lien avec la rubrique « Algorithmique »</td>
        </tr>
        <tr>
            <td>Graphes : structures relationnelles. Sommets, arcs, arêtes, graphes orientés ou non orientés.</td>
            <td>Modéliser des situations sous forme de graphes.<br>
            Écrire les implémentations correspondantes d’un graphe : matrice d’adjacence, liste de successeurs/de prédécesseurs.<br>
            Passer d’une représentation à une autre.</td>
            <td>On s’appuie sur des exemples comme le réseau routier, le réseau électrique, Internet, les réseaux sociaux.<br>
            Le choix de la représentation dépend du traitement qu’on veut mettre en place : on fait le lien avec la rubrique « Algorithmique ».</td>
    </tbody>
</table>