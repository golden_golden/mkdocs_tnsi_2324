﻿---
title : "Devoir à la maison"

---

# Devoir à la maison

La société LOCAVACANCES doit gérer la réservation de l’ensemble des chambres de ses gîtes. Chaque chambre d’un même complexe sera différenciée par son nom.



Pour cela, d’un point de vu informatique, on a créé deux classes : `Chambre` et `Gite` dont le code est donné ci-dessous.


??? example "Code"
    ```python
    class Chambre:
        def __init__(self, nom: str):
            self._nom = nom
            self._occupation = [False for i in range(365)]
    
        def get_nom(self):
            return self._nom
    
        def get_occupation(self):
            return self._occupation
    
        def reserver(self, date: int):
            self._occupation[date - 1] = True



    class Gite:
        def __init__(self, nom: str):
            self._nom = nom
            self._chambres = []
    
        def __str__(self):
            n = len(self._chambres)
            if n == 0:
                return "L'hôtel " + self._nom + " n’a aucune chambre."
            else:
                return "L’hôtel " + self._nom + " a " + str(n) + " chambre(s)"
    
        def get_chambres(self):
            return self._chambres
    
        def get_nchambres(self):
            return [ch.get_nom() for ch in self._chambres]
    
        def ajouter_chambres(self, nom_ch : str):
            self._chambres.append(Chambre(nom_ch))
    
        def mystere(self, date):
            l_ch = []
            for ch in self._chambres :
                if ch.get_occupation()[date - 1] == False :
                    l_ch.append(ch.get_nom())
            return(l_ch)
    ```


## Partie A - Étude de la classe Chambre

- Lister les attributs en donnant leur type. Préciser s’ils sont modifiables dans la classe, en explicitant la méthode associée.
  
- Écrire un `assert` dans la méthode `reserver` pour vérifier si le nombre date passé en paramètre est bien compris entre 1 et 365 (on ne gère pas les années bissextiles).
  
- Écrire la méthode `AnnulerReserver(self,  date : int)` qui annule la réservation pour le jour date.


## Partie B - Étude de la classe Gite
Le gîte « BonneNuit » a 5 chambres dénommées :
'Ch1', 'Ch2', 'Ch3', 'Ch4', 'Ch5'
On définit l’objet GiteBN par l’instruction :`GiteBN = Gite("BonneNuit")`.

- Méthode `ajouter_chambres()`
Écrire l’instruction Python pour ajouter 'Ch1' à l’objet GiteBN.
Dans les questions suivantes 2, 3 et 4, on considère que l’objet GiteBN contient toutes les chambres du gîte «BonneNuit ».
- La méthode ajouter_chambres permet d’enregistrer une nouvelle chambre, mais elle ne teste pas si le nom de cette chambre existe déjà.
Modifier la méthode pour éviter cet éventuel doublon.
- Étude des méthodes : `get_chambres()` et `get_nchambres()`
  
a) Parmi les 4 propositions ci-dessous, quel est le type renvoyé par l’instruction Python : `GiteBN.get_chambres()`

<center>
 String | Objet Chambre | Tableau de String | Tableau d’objet Chambre
        |               |                   |                         
</center>


b) Qu’affiche la suite d’instructions suivante ?

```python
Ch = GiteBN.get_chambres()[1]
print(Ch.get_nom())
```

c) Quelle différence existe-t-il entre les deux méthodes `get_nchambres()` et `get_chambres()` ?

- Les chambres 'Ch1', 'Ch3', 'Ch5' sont réservées pour tout le mois de Janvier 2021.

La méthode mystère étant précisée dans l’annexe, répondre aux questions suivantes :

- *Que va renvoyer l’instruction `GiteBN.mystere(3)` ?*
- *Dans la méthode `mystère`, quel est type des variables en paramètre et en sortie ?*

Quelles sont les méthodes ou attributs dont elle a besoin ?
