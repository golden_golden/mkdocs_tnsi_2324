---
title: "Avantages"

---



# Les avantages de la programmation orientée objet

## 1. Pourquoi, la programmation orientée objet ?

La POO est une réponse à des problèmes posés par l’industrie du logiciel en termes de complexité accrue et de stabilité dégradée.

La programmation orientée objet, essaie d'apporter des solutions à ces problématiques, à l'aide de cinq concepts fondamentaux à savoir :

- La classe
- L’objet
- l'encapsulation des attributs qui empêche toute modification externe accidentelle.
- l'héritage qui permet la ré utilisabilité du code.
- Le polymorphisme : (Cette notion ne sera pas étudiée cette année !)


## 2. Les avantages de la POO
    
!!! note "à reprendre"
    <center>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/Zpi9LAKMrlU?start=118&end=466" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </center>

    L'utilisation de ces concepts, permet de :

    - **Organiser son code :** les objets forment des modules compacts regroupant des données et un ensemble d’opérations.


    - **L’abstraction :** 
    - **Simplifier les interfaces :**  
    - **Masquer la complexité :** les objets de la POO sont proches de ceux du monde réel. Les concepts utilisés sont donc proches des abstractions familières que nous exploitons.

!!! note "Remarque"
    En POO, l'étape de modélisation revêt une importance majeure. C'est en effet elle qui permet de transcrire les éléments du réel sous forme virtuelle.

    - **Réutiliser son code :**
    - **Faciliter les ajouts :**
    - **Productivité et ré-utilisabilité :** plus l’application est complexe et plus l’approche POO est intéressante. Le niveau de ré-utilisabilité est supérieur à la programmation procédurale.  


    - **Sûreté :** l’encapsulation et le typage des classes offrent une certaine robustesse aux applications.