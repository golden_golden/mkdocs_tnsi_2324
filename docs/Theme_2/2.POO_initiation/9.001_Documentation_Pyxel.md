---
title : "Documentation Pyxel"

---


# Documentation de l’API

## Système

- **`width`, `height`**<br>
  La largeur et la hauteur de l’écran

- **`init(width, height, [title], [fps], [quit_key], [display_scale], [capture_scale], [capture_sec])`**<br>
  Initialise l’application Pyxel avec un écran de taille (`width`, `height`). Il est possible de passer comme options :

  - `title`: le titre de la fenêtre,
  - `fps`: le nombre d’images par seconde,
  - `quit_key`: la touche pour quitter l’application,
  - `display_scale`: l'échelle de l'affichage,
  - `capture_scale`: l’échelle des captures d’écran,
  - `capture_sec`: le temps maximum d’enregistrement vidéo .<br>
  

  Par exemple : `pyxel.init(160, 120, title="My Pyxel App", fps=60, quit_key=pyxel.KEY_NONE, capture_scale=3, capture_sec=0)`

- **`run(update, draw)`**<br>
  Lance l’application Pyxel et appelle la fonction `update` et la fonction `draw`.

- **`show()`**<br>
  Affiche l’écran jusqu’à ce que la touche `Esc` soit appuyée.

- **`quit()`**<br>
  Quitte l’application Pyxel.


## Entrées

- **`mouse_x`, `mouse_y`**<br>
  La position actuelle du curseur de la souris

- **`btn(key)`**<br>
  Renvoie `True` si la touche `key` est appuyée, sinon renvoie `False` ([liste des touches](../python/pyxel/__init__.pyi))

- **`btnp(key, [hold], [repeat])`**<br>
  Renvoie `True` si la touche `key` est appuyée à cette frame, sinon renvoie `False`. Quand `hold` et `repeat` sont spécifiés, `True` sera renvoyé à l’intervalle de frame `repeat` quand la touche `key` est appuyée pendant plus de `hold` frames

- **`btnr(key)`**<br>
  Renvoie `True` si la touche `key` est appuyée à cette frame, sinon renvoie `False`

- **`mouse(visible)`**<br>
  Si `visible` est `True`, affiche le curseur de la souris. Si `False`, le curseur est caché. Même si le curseur n’est pas affiché, sa position est actualisée.

## Graphiques

- **`cls(col)`**<br>
  Efface l’écran avec la couleur `col`

- **`line(x1, y1, x2, y2, col)`**<br>
  Dessine une ligne de couleur `col` de (`x1`, `y1`) à (`x2`, `y2`)

- **`rect(x, y, w, h, col)`**<br>
  Dessine un rectangle de largeur `w`, de hauteur `h` et de couleur `col` à partir de (`x`, `y`)

- **`rectb(x, y, w, h, col)`**<br>
  Dessine les contours d’un rectangle de largeur `w`, de hauteur `h` et de couleur `col` à partir de (`x`, `y`)

- **`circ(x, y, r, col)`**<br>
  Dessine un cercle de rayon `r` et de couleur `col` à (`x`, `y`)

- **`circb(x, y, r, col)`**<br>
  Dessine le contour d’un cercle de rayon `r` et de couleur `col` à (`x`, `y`)

- **`tri(x1, y1, x2, y2, x3, y3, col)`**<br>
  Dessine un triangle avec les sommets (`x1`, `y1`), (`x2`, `y2`), (`x3`, `y3`) et de couleur `col`

- **`trib(x1, y1, x2, y2, x3, y3, col)`**<br>
  Dessine les contours d’un triangle avec les sommets (`x1`, `y1`), (`x2`, `y2`), (`x3`, `y3`) et de couleur `col`


## Mathématiques

- **`ceil(x)`**<br>
  Renvoie le plus petit nombre entier supérieur ou égal à `x`.

- **`floor(x)`**<br>
  Renvoie le plus grand nombre entier inférieur ou égal à `x`.

- **`sgn(x)`**<br>
  Renvoie 1 lorsque `x` est positif, 0 lorsqu'il est nul, et -1 lorsqu'il est négatif.