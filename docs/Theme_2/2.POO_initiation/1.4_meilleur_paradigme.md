---
title: "Meilleur paradigme"

---

# Quel est le meilleur paradigme de programmation ?


??? note "métaphore"
    Imaginons 3 menuisiers qui ont pour mission de fabriquer chacun un meuble.

    - Le premier pourra décider d'utiliser du collé-pointé : il assemblera les morceaux de bois en les collant puis utilisera des pointes. Ses outils seront le marteau et le pistolet à colle.
    - Le deuxième pourra décider de visser les morceaux de bois entre eux : son outil principal sera une visseuse.
    - Le troisième pourra décider de faire de l'assemblage par tenons et mortaises : son outil principal sera une défonceuse.
  
    Pour la réalisation de sa mission, chaque menuisier utilise un paradigme différent. Qui utilise la meilleure méthode ? Cette question n'a pas vraiment de réponse : certaines méthodes sont plus rapides que d'autres, d'autres plus robustes, d'autres plus esthétiques…
    Et pourquoi ne pas mélanger les paradigmes ? Rien n'interdit d'utiliser des pointes ET des vis dans la fabrication d'un meuble.



Il est inconséquent d’opposer la Programmation Orientée Objet aux autres paradigmes de programmation, car la Programmation Orientée Objet sera, surtout à notre niveau, mélangée avec de la programmation impérative, de la programmation fonctionnelle… Toute la programmation des méthodes reste tributaire des mécanismes de programmation procédurale et structurée ; on y rencontre des variables, des arguments, des boucles, des arguments de fonction, des instructions conditionnelles, tout ce que l’on trouve classiquement dans les boîtes à outils impératives.







