'''Fichier relatif au cours sur les classes.
'''


class Point:
    "Documentation de la classe point"
    x = "valeur définie dans la classe"
    y = "valeur définie dans la classe"


pt_A = Point()
pt_B = Point()


def print_objet():
    print("print()")
    print(pt_A)
    print(pt_B)
    print(Point())


def espace_nommage():
    print("espace de nomage avec __dict__")
    print(pt_A.__dict__)
    print(pt_B.__dict__)
    print(Point.__dict__)


def print_instance():
    print("point A : x =", pt_A.x, "; y =", pt_A.y)
    print("point B : x =", pt_B.x, "; y =", pt_B.y)


def attribution_A():
    pt_A.x = 3
    pt_A.y = 2
    pt_A.nom = "Point A"

def risque_bug():
    Point.x = "attribut de classe modifé !"
    print(pt_B.x)


attribution_A()
print_instance()
espace_nommage()


risque_bug()
espace_nommage()