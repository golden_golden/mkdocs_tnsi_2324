import pyxel

HAUTEUR_FENETRE = 200
LARGEUR_FENETRE = 120
TITRE = "mon jeu"


class Jeu:

    def __init__(self):
        # Création de la fenêtre
        pyxel.init(LARGEUR_FENETRE, HAUTEUR_FENETRE, title=TITRE)

        self.carre = Rectangle(dim_horizontal=20)

        # Lancement du jeu$
        pyxel.run(self.update, self.draw)  # A METTRE EN FIN DE CONSTRUCTEUR

    def update(self):
        """mise à jour des variables (30 fois par seconde)"""
        self.carre.deplacer()
        self.carre.tourner()

    def draw(self):
        """création et positionnement des objets (30 fois par seconde)"""
        pyxel.cls(0)
        self.carre.afficher()


class Rectangle:
    def __init__(self, x=(LARGEUR_FENETRE-10)/2, y=(HAUTEUR_FENETRE-10)/2, dim_vertical=10, dim_horizontal=10, couleur=4):
        self.x = x
        self.y = y
        self.vx = 2
        self.vy = 1
        self.dim_vertical = dim_vertical
        self.dim_horizontal = dim_horizontal
        self.couleur = couleur

    def afficher(self):
        pyxel.rect(self.x, self.y, self.dim_horizontal,
                   self.dim_vertical,  self.couleur)
        
    def deplacer(self):
        # Gestion des rebonds sur les parois : on inverse le sens de déplacement
        if self.x < 1 or self.x > (LARGEUR_FENETRE-self.dim_horizontal): # Bords verticaux ( gauche et droite )
            self.vx = - self.vx
        if self.y < 1 or self.y > (HAUTEUR_FENETRE-self.dim_vertical): # Bords horizontaux ( haut et bas )
            self.vy = - self.vy
        
        # Mise à jour des coordonnées du carré
        self.x += self.vx
        self.y += self.vy
        
    def tourner(self):
        if pyxel.btn(pyxel.KEY_RIGHT):
            self.dim_vertical, self.dim_horizontal = self.dim_horizontal, self.dim_vertical

Jeu()
