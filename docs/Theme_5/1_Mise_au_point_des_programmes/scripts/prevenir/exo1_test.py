assert correspond('INFORMATIQUE', 'INFO*MA*IQUE') == True
"Test donné : 'INFORMATIQUE' et  'INFO*MA*IQUE' correspondent !"
assert correspond('AUTOMATIQUE', 'INFO*MA*IQUE') == False
"Test donné : 'AUTOMATIQUE' et  'INFO*MA*IQUE' correspondent !"
assert correspond('AUTOMATIQUE', 'AUTO') == False
"deux mot de longueur différent ne peuvent pas correspondre !"
assert correspond('AUTO', 'AUTOMATIQUE') == False
"deux mot de longueur différent ne peuvent pas correspondre !"
assert correspond('AUTOMATIQUE', '*UTOMATIQUE') == True
"AUTOMATIQUE' et  '*UTOMATIQUE' correspondent, est ce parce que * est au début ?"
assert correspond('AUTOMATIQUE', 'AUTOMATIQU*') == True
"AUTOMATIQUE' et  'AUTOMATIQU*' correspondent, est ce parce que * est au début ?"