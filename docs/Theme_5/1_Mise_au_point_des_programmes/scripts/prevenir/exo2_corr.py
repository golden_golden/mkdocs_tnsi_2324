def moyenne(resultats):
    numerateur = 0
    denominateur = 0
    for resultat in resultats:
        (note, coefficient) = resultat
        numerateur += (note * coefficient)
        denominateur += coefficient
    return numerateur / denominateur


# Voici une solution envisageable, mais il existe certainement d'autres solutions !
# Cette solution ne présente pas : les annotations de type, la docstring, et les assertions !
