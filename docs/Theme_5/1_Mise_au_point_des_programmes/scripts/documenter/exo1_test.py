assert recherche("a", "abcde") == 1
"Test où le caractère recherche est en première place : il y à 1 `a` dans `abcde`"
assert recherche("a", "bcdea") == 1
"Test où le caractère recherche est en dernière place : il y à 1 `a` dans `abcde`"
assert recherche("a", "aaaaaaaaaa") == 10
"Test où tous les caractères sont le caractère recherché : il y à 10 `a` dans `aaaaaaaaaa`"
assert recherche("e", "sciences") == 2
"Il y a 2 e dans sciences !"
assert recherche("i", "mississippi") == 4
"Il y a 4 e dans mississippi !"
assert recherche("a", "mississippi") == 0
"Il y a 0 a dans mississippi !"
