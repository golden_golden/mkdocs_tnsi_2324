---
title : "Langages et programmation"

---

# Langages et programmation

## Sommaire 

1. Mise au point des programmes *(Bac)*

2. Modularité *(Bac)*
   
3. Récursivité (Bac)
   
4. Notion de programme en tant que donnée
   
5. Paradigmes de programmation
   
6. Calculabilité, décidabilité
   