---
title : "Programme"

---

# Langages et programmation

*Extrait du programme*

Le travail entrepris en classe de première sur les méthodes de programmation est prolongé. L’accent est mis sur une programmation assurant une meilleure sûreté, c’est-à-dire minimisant le nombre d’erreurs. Parallèlement, on montre l’universalité et les limites de la notion de calculabilité.

La récursivité est une méthode fondamentale de programmation. Son introduction permet également de diversifier les algorithmes étudiés. En classe terminale, les élèves s’initient à différents paradigmes de programmation pour ne pas se limiter à une démarche impérative.

<br>

<table>
    <thead>
        <tr>
            <th >Contenus</th>
            <th >Capacités attendues</th>
            <th >Commentaires</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Notion de programme en tant que donnée.<br>
            Calculabilité,décidabilité.</td>
            <td>Comprendre que tout programme est aussi une donnée.<br>
            Comprendre que la calculabilité ne dépend pas du langage de programmation utilisé.<br>
            Montrer, sans formalisme théorique, que le problème de l’arrêt est indécidable.</td>
            <td>L’utilisation d’un interpréteur ou d’un compilateur, le téléchargement de logiciel, le fonctionnement des systèmes d’exploitation permettent de comprendre un programme comme donnée d’un autre programme.</td>
        </tr>
        <tr>
            <td>Récursivité.</td>
            <td>Écrire un programme récursif.<br>
            Analyser le fonctionnement d’un programme récursif.</td>
            <td>Des exemples relevant de domaines variés sont à privilégier.</td>
        </tr>
        <tr>
            <td>Modularité.</td>
            <td>Utiliser des API (Application Programming Interface) ou des bibliothèques.<br>
            Exploiter leur documentation.<br>
            Créer des modules simples et les documenter.</td>
            <td></td>
        </tr>
        <tr>
            <td>Paradigmes de programmation.</td>
            <td>Distinguer sur des exemples les paradigmes impératif, fonctionnel et objet.<br>
            Choisir le paradigme de programmation selon le champ d’application d’un programme.</td>
            <td>Avec un même langage de programmation, on peut utiliser des paradigmes différents. Dans un même programme, on peut utiliser des paradigmes différents.</td>
        </tr>
        <tr>
            <td>Mise au point des programmes.<br>
            Gestion des bugs</td>
            <td>Dans la pratique de la programmation, savoir répondre aux causes typiques de bugs : problèmes liés au typage, effets de bord non désirés, débordements dans les tableaux, instruction conditionnelle non exhaustive, choix des inégalités, comparaisons et calculs entre flottants, mauvais nommage des variables, etc.</td>
            <td>On prolonge le travail entrepris en classe de première sur l’utilisation de la spécification, des assertions, de la documentation des programmes et de la construction de jeux de tests.<br>
            Les élèves apprennent progressivement à anticiper leurs erreurs.</td>
        </tr>
    </tbody>
</table>