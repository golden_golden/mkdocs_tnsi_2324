import aires

choix = int(input("Calcul de l'aire d'un triangle (choix 1) ou d'un rectangle (choix 2) :"))
if choix == 1 :
    base = float(input("Entrer la base du triangle :"))
    hauteur = float(input("Entrer la hauteur du triangle :"))
    print("L'aire d'un triangle de base ", base, "et de hauteur ", hauteur, "est égale à ", aires.aire_triangle(base, hauteur))
else : 
    longueur = float(input("Entrer la longueur du rectangle :"))
    largeur = float(input("Entrer la largeur du rectangle :"))
    print("L'aire d'un rectangle de longueur ", longueur, "et de largeur ", largeur, "est égale à ", aires.aire_rectangle(longueur, largeur))
