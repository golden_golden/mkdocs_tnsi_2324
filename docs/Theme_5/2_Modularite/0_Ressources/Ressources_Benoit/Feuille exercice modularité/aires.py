'''Le module aires met à disposition des fonctions
permettant de calculer l'aire de figures usuelles :
triangle, disque, rectangle, carré, parallélogramme'''

import math

def aire_triangle(base, hauteur):
    '''aire_triangle renvoie l'aire d'un triangle
    Entrées
    -------
    base : un nombre égal à la longueur de la base du triangle
    hauteur : un nombre égal à la hauteur du triangle

    Sortie
    --------
    un nombre
    '''
    return base*hauteur/2

def aire_disque(rayon):
    '''aire_disque renvoie l'aire d'un disque
    Entrées
    -------
    rayon : un nombre égal au rayon du disque

    Sortie
    --------
    un nombre
    '''
    return math.pi*rayon**2

def aire_rectangle(longueur, largeur):
    '''aire_rectangle renvoie l'aire d'un rectangle
    Entrées
    -------
    longueur : un nombre égal à la longueur du rectangle
    largeur : un nombre égal à la largeur du rectangle

    Sortie
    --------
    un nombre
    '''
    return longueur*largeur
