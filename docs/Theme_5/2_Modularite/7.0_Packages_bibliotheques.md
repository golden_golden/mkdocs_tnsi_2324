---
title : "Packages et bibliothèques"

---

# Packages et bibliothèques

## 1. Package

Quand on a un grand nombre de modules, il peut être intéressant de les regroupés autour de thème précis et de les organiser dans des dossiers. 

!!! def "Package"
    Un dossier qui rassemble des modules est appelé un **package** (paquetage en français). Le nom du package est le même que celui du dossier.

<br/>

Par exemple, on crée un dossier `math2` dans lequel on place le fichier `fonctions_usuelles.py`, dans le quel se trouve la fonction suivante :

```python
    def carre(x):
        return x**2
```

<br/>

On peut ensuite utiliser la fonction `carre(x)` définie dans `fonctions_ussuelles.py`, en important `math2.fonction_usuelle` comme dans l’exemple qui suit :

```python
    import math2.fonctions_usuelles

    u= math2.fonctions_usuelles.carre(3)
    print(u)
```

```python
    9
```


??? note "Remarque"
    Il est aussi possible d’avoir des dossiers imbriqués, c’est-à-dire des dossiers qui contiennent d’autres dossiers.


<br/>

## 2. Bibliothèque
!!! def "Bibliothèque"
    Une bibliothèque est constituée de plusieurs package.

On utile également le mot *library* pour désigner une bibliothèque.


