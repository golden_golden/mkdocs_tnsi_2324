---
title: "Création d’un module"

---

# Création d’un module

## 1. Mon premier module

En Python, la création d'un module est très simple !

- On crée un fichier nommée **nom.py**
   
- On implémente les fonctions ou procédure que l’on veut dans le fichier.


!!! example "Exemple"

	Nous allons essayer de créer notre propre module Python nommée **fonctions_usuelles:**

	- On crée un fichier nommée **fonctions_usuelles.py**
   
	- On implémente les fonctions ou procédure que l’on veut dans le fichier **fonctions_usuelles.py**
   
	```python
	    """
	    Module fonctions_usuelles
	    Version : 0.1
	    Auteur : toto
	    Remarque : Mon permier module
	    """

	    def somme(x,y):
	    	"""
	    	additionne de deux nombre et renvoie le résultat.
	
	    	:param a: Le permier nombre à additionner.
	    	:param b: Le second nombre à additionner.
	    	:type a: int
	    	:type b: int
	    	:return: Le résultat de l'addition
	    	:rtype: int

	    	:Example:
	
	    	>>> add(1, 2)
	    	3
	    	"""
	    	return x + y

	    def division(numerateur,denominateur):
	    	""" Documentation de la fonction division """
	    	return numerateur/denominateur

	    def carre(x):
	    	""" Docuentation de la fonction carré"""
	    	return x**2
	``` 

- On oublie surtout pas d’écrire des *docstring* pour le module et pour chacune des fonctions présentes dans le module. *(Cette étape est très importante, comme vous pourrez le voir dans la partie sur les API et la documentation.)*

- On oublie pas d’enregistrer le fichier.


Et voilà, vous venez de créer votre premier module !


## 2. Visibilité des fonctions dans un module

La visibilité des fonctions au sein des modules suit des règles simples :

- Les fonctions dans un même module peuvent s'appeler les unes les autres.

- Certaine fonction présente dans un module, ne sont là que pour être utilisée par d’autres fonction du module et pas par les futurs utilisateurs du module. On parle alors de *fonction privé*. Par convention dans python, leurs nom sera précédé d’un underscore “_”. Par exemple, ` _ma_fontion_privé()`.

- Les fonctions dans un module peuvent appeler des fonctions situées dans un autre module s'il a été préalablement importé. Par exemple, si  la commande `import autremodule` est utilisée dans un module, il est possible d'appeler une fonction avec `autremodule.fonction()`.
