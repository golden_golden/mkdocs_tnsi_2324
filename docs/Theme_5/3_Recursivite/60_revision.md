---
title : "Révisions"

---

# Fiche de révision sur la récursivité

## 1. Qu'est-ce que la programmation récursive

!!! note "Définition"
    On appelle récursive toute fonction ou procédure qui s’appelle elle même.



## 2. Qu'est-ce que la programmation récursive

Attention ! Pour qu’une fonction récursive se termine, il faut que sa structure le lui permettent.

!!! note ""
    **Règle 1 :** 

    Tout algorithme récursif doit distinguer plusieurs cas dont l’un au moins ne doit pas contenir d’appels récursifs. sinon il y a risque de cercle vicieux et de calcul infini.

    <br/>

    **Règle 2 :** 

    Tout appel récursif doit se faire avec des données plus proches de données satisfaisant les conditions de terminaison.



```python
def somme(n):
    if n==0:
        return 0
    else:
        return n + somme(n-1)
```



## 3. Les différents types de récursivité

Il existe plusieurs type de récursivité : Récursivité simple, Récursivité multiple, Récursion imbriquée, Récursion mutuelle ou croisée.



## 4. Récursivité terminale

!!! note "Définition"
    On dit qu’un fonction est récursive terminale, si tout appel récursif est de la forme return f(...).



Pour rendre terminal un algorithme récursif, on utilise un accumulateur, passé en paramètre, pour calculer le résultat au fur et à mesure des appels récursifs. La valeur de retour du cas de base devient la valeur initiale de l’accumulateur et lors d’un appel récursif, le “calcul en attente” sert à calculer la valeur suivante de l’accumulateur.

```python
def somme(n,acc=0):
    if n == 0:
        return acc
    else:
        return somme(n - 1, acc + n)
```



## 5. De récursifs à itératifs

!!! note "Propriété"
    Tout algorithme récursif peut être transformé en un algorithme itératif.



## 6. Avantages et inconvénients des algorithmes récursifs

### A. Les inconvénients 

- Les fonctions récursives requièrent généralement plus d'espace mémoire, ce qui peut dans certain cas devenir rédhibitoire ; *(Attention, à la limitation de 1000 appels récursifs en Python).*

- Une fonction récursive peut être de plus grande compacité dans son écriture mais n'est pas forcément de plus petite complexité temporelle. Le nombre de calculs effectués est souvent bien plus grand qu'en programmation itérative.

  

### B. Les avantages

- La récursivité offre au programmeur un autre moyen, souvent élégant et concis, de résoudre certains problèmes.

- La récursivité permet de traduire simplement certaines opérations mathématiques ; 
- La récursivité permet des résonnements plus naturel lors du travail sur certaines structure de données, tel que : liste, arbre, graphe.
- La compacité des algorithmes récursifs facilite la lisibilité et le débogage.



## 7. Alors itératif ou récursif ?

Le choix d'une version récursive ou itérative d'un programme, doit se faire selon plusieurs critères, les performances, bien sur, mais aussi celui de la facilité d'implémentation. Cependant, si on recherche l'efficacité *(une exécution rapide)*,  et qu’il est possible d’écrire un algorithme itératif sans trop de difficultés, on préférera la version itérative !

<br/>

Il est tout à fait possible de mélanger des parties récursives et des parties itératives.
