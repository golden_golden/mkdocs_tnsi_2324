---
title : "Trace d'exécution"

---


# Trace d’exécution

## 1 Rappel trace d'exécution

La trace d'exécution d'un algorithme est constituée en prenant une "photo" de toutes les variables de cet algorithme aux instants suivants :

- au début
- à chaque `while` ou a chaque appel de fonction
- à la fin

La trace est un "compte-rendu" de l'exécution de l'algorithme.



## 2 - Étude de la fonction factorielle

Une fonction peut exécuter plusieurs appels récursifs. L’exécution d’une fonction récursive est basée sur une pile de le mémoire vive.

!!! example "Exemple"
    ```python
    def fact(n):
        if n == 0:
            return(1)
        else:
            return n*fact(n-1)

    print(fact(3))
    ```

    ```python
    6
    ```



    **Trace d’exécution**


    >  Appel de la fonction fact(3)
    >
    >  - Mise en mémoire du calcul : 3 × fact(3-1)
    >
    >  - Appel de la fonction fac(2)
    >
    >    - Mise en mémoire du calcul : 2 × fac(2-1)
    >
    >    - Appel de la fonction fac(1)
    >
    >      - Mise en mémoire du calcul : 1 × fac(1-1)
    >
    >      - Appel de fac(0)
    >
    >        
    >
    >      - Renvoie la valeur 1
    >
    >  - Effectue le calcul et renvoie la valeur 1
    >
    >  - Effectue le calcul et renvoie la valeur 2
    >
    >  Effectue le calcul et renvoie la valeur 6
    >
    >  Affichage de la valeur 6



## 3 - Pile d’exécution

!!! note "**Définition :** Pile d’exécution"
    La pile d’exécution du programme en cours est un emplacement mémoire destiner à mémoriser les paramètres, les variables locales ainsi que les valeurs de retour des fonctions en cours d’exécution. Lorsqu’une fonction a terminé son exécution, elle est dépilée.



Elle fonctionne selon le principe LIFO (Last-In-First-Out) : dernier entré premier sorti. 

<br/>

Chaque appel récursif nécessite de construire dynamiquement un nouvel environnement , un nouvel appel de fonction :

- adresse(s) de retour

- paramètres d’appel

- variables locales

  


L’exécution d’un appel récursif passe pur deux phases, le phase de descente et la phase de remontée :

<br/>

Dans la phase de descente, chaque appel récursif fait à sont tour un appel récursif.

<br/>

En arrivant à la condition terminale, on commence la phase de remontée qui se poursuit jusqu’à ce que l’on appel initial soit terminé, ce qui termine le processus récursif.


<center>
![Trace d'exécution](./image/fibo1.png){width="1000" loading="lazy"}
</center>

<center>
![Trace d'exécution](./image/fibo2.png){width="1000" loading="lazy"}
</center>




??? note "Remarque"
    Concrètement, ce qui est stocké à chaque étage de la pile, c'est l'adresse mémoire de l'instruction à exécuter. Pour simplifier, ici c'est l'instruction qui est écrite.



??? note "Remarque"
    Le langage Python génère et gère automatiquement les espaces-mémoires de la partie dédiée de la mémoire physique de  l’ordinateur : la pile d'exécution (ou pile de récursivité).
    
    Comme tout système physique, sa capacité est limitée.  Par défaut, l'implémentation de Pyhton limite la hauteur de la pile de  récursivité à 1000. 

    Si l'exécution d'un appel récursif conduit à vouloir dépasser cette hauteur maximale, alors le message d'erreur : `RuntimeError: maximum recursion depth exceeded in comparison` apparaît.            


    ```python
    >>> fact(1000)
    ...
    RecursionError: maximum recursion depth exceeded in comparison
    ```


    Il est possible de connaître et de modifier la hauteur limite de la pile de récursivité sous Python. 
    Pour cela, il suffit d'utiliser la bibliothèque `sys` qui permet de gérer, entre autres, des propriétés du système. Cette bibliothèque contient :

    - Une fonction `getrecursionlimit` qui n'a pas d'argument et renvoie la taille limite de la pile de récursivité.
    - 
    ```python
    >>> import sys
    >>> sys.getrecursionlimit()
    1000
    ```


    - Une fonction `setrecursionlimit` qui admet comme argument un nombre entier correspondant à la taille limite voulue pour la pile de récursivité.

    ```python
    >>> sys.setrecursionlimit(1500)
    >>> fac(1000)
    40238...000
    >>> len(str(fac(1000)))
    2568
    ```

    Toutefois, comme indiqué dans la documentation, pour des raisons de portabilité, on modifiera avec prudence le plafond des appels. En outre, même si on le lève, un crash peut se produire :

    <img src="C:\Users\Arnaud\AppData\Roaming\Typora\typora-user-images\image-20201030191817996.png" alt="image-20201030191817996" style="zoom: 50%;" />

    On remarquera que le crash est grave au point qu’il a échappé au système d’exceptions de Python.
