---
title : "Récursivité Terminale"

---


# Récursivité terminale

## 1. Récursivité terminale


!!! note "Définition"
    On dit qu’un fonction est récursive terminale, si tout appel récursif est de la forme return f(...).



Autrement dit, la valeur retournée est directement la valeur obtenue par un appel récursif, sans qu’il n’y ait aucune opération sur cette valeur. L’avantage est qu’il n’y a pas de “calcul en attente”, il n’y a ainsi rien à mémoriser dans la pile.


!!! example "Exemple"
    === "Récursive terminale"
        ```python
        def pgcd (a , b ) :
            if b == 0:
                return a
            else :
                return pgcd (b , a % b )

        print(pgcd(150,130))
        ```

        Cette fonction est récursive terminale

    === "Récursive pas terminale"
        ```python
        def fac(n):
            if n == 0:
                return 1
            else:
                return n * fac(n-1)

        print(fac(3))
        ```

        Cette fonction n’est pas récursive terminale.



## 2. Rendre terminal un algorithme récursif

Pour rendre terminal un algorithme récursif, on utilise un accumulateur, passé en paramètre, pour calculer le résultat au fur et à mesure des appels récursifs. La valeur de retour du cas de base devient la valeur initiale de l’accumulateur et lors d’un appel récursif, le “calcul en attente” sert à calculer la valeur suivante de l’accumulateur.



!!! example "Exemple"

    Cette fonction n’est pas récursive terminale.

    ```python
    def fac(n):
        if n == 0:
            return 1
        else:
            return n * fac(n - 1)

    print(fac(3))
    ```

    Mais on peut créer une fonction récursive terminale, en utilisant un accumulateur.

    ```python
    def fac(n, acc=1):
        if n == 0:
            return acc
        else:
            return fac(n - 1, acc * n)

    print(fac(3))
    ```

    Cette fonction est récursive terminale !



## 3. Mais pourquoi utiliser la récursivité terminale

!!! note "Retenir"
    Quand une fonction est récursive terminale, on peut transformer l’appelle récursif en une boucle, sans utilisation de mémoire supplémentaire.



Attention ! Cette optimisation n’est pas supportée dans tout les langages ! 

*(Python ne supporte pas cette optimisation !)*


